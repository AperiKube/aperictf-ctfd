function updatescores () {
	$.get(script_root + '/api/v1/scoreboard', function (response) {
		var teams = response.data;
		var table = $('#scoreboard tbody');
		table.empty();
        //console.dir(teams);
		for (var i = 0; i < teams.length; i++) {
			var row = "<tr>\n" +
				"<th scope=\"row\" class=\"text-center\">{0}</th>".format(i + 1) +
				"<td><a href=\"{0}/teams/{1}\">{2}</a></td>".format(script_root, teams[i].account_id, htmlentities(teams[i].name)) +
				"<td>{0}</td>".format(teams[i].score) +
				"</tr>";
			table.append(row);
		}
	});
}

function cumulativesum (arr) {
	var result = arr.concat();
	for (var i = 0; i < arr.length; i++){
		result[i] = arr.slice(0, i + 1).reduce(function(p, i){ return p + i; });
	}
	return result
}

//setInterval(update, 300000); // Update scores every 5 minutes
//scoregraph();

/********************************/

chartColors = [
	'rgb(255, 99, 132)',
	'rgb(255, 159, 64)',
	'rgb(255, 205, 86)',
	'rgb(75, 192, 192)',
	'rgb(54, 162, 235)',
	'rgb(153, 102, 255)',
	'rgb(201, 203, 207)',
	'rgb(255, 99, 132)',
	'rgb(255, 159, 64)',
	'rgb(255, 205, 86)'
];

var lineChartData = {
	datasets: [{
		label: 'Équipe 1',
		borderColor: chartColors[0],
		backgroundColor: chartColors[0],
		fill: false,
		data:  []
	},{
		label: 'Équipe 2',
		borderColor: chartColors[1],
		backgroundColor: chartColors[1],
		fill: false,
		data: []
	}]
};

var configctx = {
	data: lineChartData,
	options: {
		elements: {
			line: {
				tension: 0.2
			}
		},
		responsive: true,
		hoverMode: 'index',
		stacked: false,/*
		title: {
			display: true,
			text: 'Chart.js Line Chart - Multi Axis'
		},*/
		scales: {
			yAxes: [{
				type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
				display: true,
				position: 'left',
				id: 'y-axis-1',
				ticks: { beginAtZero: true }
			}],
			xAxes: [{
				type: 'time',
				time: {
					tooltipFormat: 'D MMM h:mm',
					scaleBeginAtZero : true,
					max: new Date()
				}
			}]
		}
	}
}

window.onload = function() {
	var ctx = document.getElementById("scoreChart").getContext('2d');
	window.myLine = Chart.Line(ctx, configctx);
};


function updateGraph(){
	$.get(script_root + '/api/v1/scoreboard/top/10', function( response ) {
		var places = response.data;
		if (Object.keys(places).length === 0 ){
			// Replace spinner
			//$('#score-graph').html(
			//	'<div class="text-center"><h3 class="spinner-error">No solves yet</h3></div>'
			//);
			return;
		}

		var teams = Object.keys(places);
		var traces = [];
		lineChartData.datasets = [];
		
		
		// Mindate = Date d'ouverture du CTF
		var mindate = new Date();
		for(var i = 0; i < teams.length; i++){  // Pour chaque équipe
            console.log(i);
			for(var j = 0; j < places[teams[i]]['solves'].length; j++){
				var d = moment(places[teams[i]]['solves'][j].date).toDate();
				
				if (d < mindate){
					mindate = d;
				}
			}
		}

        lineChartData.datasets = [];
		//console.dir(mindate);
		for(var i = 0; i < teams.length; i++){  // Pour chaque équipe
			var team_score = [];
			var times = [];
			var ddata = []
			
			ddata.push({
				x:mindate, // Debut du CTF
				y:0 // Debut du CTF
			});

			//console.dir(places[teams[i]]['solves']);
			for(var j = 0; j < places[teams[i]]['solves'].length; j++){
				team_score.push(places[teams[i]]['solves'][j].value);
			}
			team_score = cumulativesum(team_score);

			for(var j = 0; j < places[teams[i]]['solves'].length; j++){
				var date = moment(places[teams[i]]['solves'][j].date);
				ddata.push({x:date.toDate(),y:team_score[j]});
			}

			var newDataset = {
				label: places[teams[i]]["name"],
				backgroundColor: chartColors[i],
				borderColor: chartColors[i],
				data: ddata,
				fill: false
			};
			lineChartData.datasets.push(newDataset);
			window.myLine.update();
		}
	});
}

/************************/

function update(){
  updatescores();
  updateGraph();
}
setInterval(update, 300000); // Update scores every 5 minutes
update();

